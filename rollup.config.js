import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import typescript from '@rollup/plugin-typescript';
import css from 'rollup-plugin-css-only';
import livereload from 'rollup-plugin-livereload';
import svelte from 'rollup-plugin-svelte';
import { terser } from 'rollup-plugin-terser';
import sveltePreprocess from 'svelte-preprocess';


const development = process.env.ROLLUP_WATCH === 'true';


export default {
	input: 'src/main.ts',
	output: {
		sourcemap: development,
		format: 'esm',
		name: 'app',
		dir: 'public/build'
	},
	plugins: [
		svelte({
      preprocess: sveltePreprocess({ sourceMap: development }),
			compilerOptions: {
				dev: development
			}
		}),
		css({ output: 'bundle.css' }),
		resolve({
			browser: true,
			dedupe: ['svelte']
		}),
		commonjs(),
    typescript({
      sourceMap: development,
      inlineSources: development,
    }),
		development && livereload('public'),
		!development && terser()
	],
	watch: {
		clearScreen: false
	}
};
