import { writable } from 'svelte/store';

function createIdCounter() {
  const { subscribe, set, update } = writable(1);

  return {
    subscribe,
    increment: () => update(n => n + 1),
  };
}

export const columnIdCounter = createIdCounter();
export const cardIdCounter = createIdCounter();
