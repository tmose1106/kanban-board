/* Implementation taken from https://github.com/kaisermann/svelte-css-vars
 * because I couldn't get yarn to add the GitHub package
 */
export function dynamicStyleVars(node, props) {
  Object.entries(props).forEach(([key, value]) => {
    node.style.setProperty(`--${key}`, value);
  });

  return {
    update(new_props) {
      Object.entries(new_props).forEach(([key, value]) => {
        node.style.setProperty(`--${key}`, value);
        delete props[key];
      });

      Object.keys(props).forEach(name =>
        node.style.removeProperty(`--${name}`),
      );
      props = new_props;
    },
  };
};

export function isUrlValid(urlString) {
  try {
    new URL(urlString);
  } catch {
    return false;
  }

  return true;
}

// Copied from StackOverflow because I liked it
// https://stackoverflow.com/questions/66619136/how-does-this-recursion-work-when-deep-clone
export function clone(object) {
  switch(object?.constructor) { 
    case Array:
      return object.map(v => clone(v))
    case Object:
      return Object
        .entries(object)
        .reduce((r, [k, v]) => Object.assign(r, {[k]: clone(v)}), {})
    default:
      return object
  }
}

/**
 * Simple object check.
 * @param item
 * @returns {boolean}
 */
export function isObject(item) {
  return (item && typeof item === 'object' && !Array.isArray(item));
}

/**
 * Deep merge two objects.
 * @param target
 * @param ...sources
 */
export function mergeDeep(target, ...sources) {
  if (!sources.length) return target;
  const source = sources.shift();

  if (isObject(target) && isObject(source)) {
    for (const key in source) {
      if (isObject(source[key])) {
        if (!target[key]) Object.assign(target, { [key]: {} });
        mergeDeep(target[key], source[key]);
      } else {
        Object.assign(target, { [key]: source[key] });
      }
    }
  }

  return mergeDeep(target, ...sources);
}

export class ChainMap {
    constructor(...maps) {
        this.maps = maps.length === 0 ? [{}] : maps.filter(isObject);
    }

    has(key) {
        for (const map of this.maps) {
            if (key in map) return true;
        }

        return false;
    }

    _get(maps, key) {
        for (const map of maps) {
            const value = map[key];

            if (value !== null && value !== undefined) return value;
        }

        return undefined;
    }

    get(key) {
        return this._get(this.maps, key);
    }

    getChainMap(key) {
        const objectsUnderKey = this.maps.map((map) => map[key]).filter(isObject);

        return new ChainMap(...objectsUnderKey);
    }

    getNested(...keys) {
        let maps = this.maps;

        for (const key of keys.slice(0, -1)) {
            maps = maps.map((map) => map[key]).filter((map) => map !== undefined && isObject(map));
        }

        if (maps.length === 0) return undefined;

        const lastKey = keys[keys.length - 1];

        return this._get(maps, lastKey);
    }

    *keys() {
        const objKeyArrays = this.maps.map((obj) => Object.keys(obj));

        const keySet = new Set([].concat.apply([], objKeyArrays));

        for (const key of keySet) {
            yield key;
        }
    }

    *values() {
        for (const key of this.keys()) {
            yield this.get(key);
        }
    }
}

