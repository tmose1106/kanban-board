import { writable } from 'svelte/store';

export enum ToastMessageType {
    Info,
    Success,
    Failure,
}

interface ToastSpec {
    message: string,
    type: ToastMessageType,
    timeout?: number,
}

interface ToastMessage extends ToastSpec {
    id: number;
}

let idCounter = 0;

export const toasts = writable([] as ToastMessage[]);

export function addToast (spec: ToastSpec): void {
    const toastId = idCounter;

    toasts.update((all) => [{id: toastId, ...spec}, ...all]);
    
    idCounter += 1;

    if (spec.timeout !== undefined) {
        setTimeout(() => dismissToast(toastId), spec.timeout);
    }
}

export function dismissToast(id: Number): void {
    toasts.update((all) => all.filter((toast) => toast.id !== id));
}
