import { isUrlValid } from '../../../util.js';

import { BackgroundSize } from '../constants.js';


interface ColorValues {
    color?: string;
    opacity?: number;
}

interface ImageValues {
    url?: string;
    offset?: number;
    size?: BackgroundSize;
    repeat?: boolean;
}

interface BoardStyleValues {
    background?: ColorValues;
    backgroundImage?: ImageValues;
    foreground?: ColorValues;
}

interface ComponentStyleValues {
    background?: ColorValues;
    backgroundImage?: ImageValues;
    foreground?: ColorValues;
    border?: ColorValues;
    font?: ColorValues;
}

interface BoardComponentStyleValues {
    board?: ComponentStyleValues;
    column?: ComponentStyleValues;
    card?: ComponentStyleValues;
}


export function getColorVariables(prefix: string, style: ColorValues): object {
    const styleVariables = {};

    if (style.color !== undefined) {
        const opacityValue = style.opacity ?? 255;

        styleVariables[`${prefix}-color`] = style.color + (opacityValue !== 255 ? opacityValue.toString(16).padStart(2, '0') : '');
        // styleVariables[`${prefix}-color`] = style.color;
    }

    return styleVariables;
}

export function getImageVariables(prefix: string, style: ImageValues): object {
    const styleVariables = {};

    if (style.url !== undefined && isUrlValid(style.url)) {
        styleVariables[`${prefix}-url`] = `url(${style.url})`;

        styleVariables[`${prefix}-offset`] = `${style.offset ?? 0}%`;

        switch (style.size) {
            case BackgroundSize.Cover:
                styleVariables[`${prefix}-size`] = 'cover';
                break;
            case BackgroundSize.Contain:
                styleVariables[`${prefix}-size`] = 'contain';
                break;
        }

        styleVariables[`${prefix}-repeat`] = style.repeat ? 'repeat' : 'no-repeat';
    }

    return styleVariables;
}

export function getBoardStyleVariables(prefix: string, style: ComponentStyleValues) {
    return {
      ...getColorVariables(`${prefix}-background`, style.background ?? {}),
      ...getImageVariables(`${prefix}-background-image`, style.backgroundImage ?? {}),
      ...getColorVariables(`${prefix}-foreground`, style.foreground ?? {}),
    };
}

export function getBoardComponentStyleVariables(prefix: string, style: ComponentStyleValues) {
    return {
      ...getColorVariables(`${prefix}-background`, style.background ?? {}),
      ...getImageVariables(`${prefix}-background-image`, style.backgroundImage ?? {}),
      ...getColorVariables(`${prefix}-foreground`, style.foreground ?? {}),
      ...getColorVariables(`${prefix}-border`, style.border ?? {}),
      ...getColorVariables(`${prefix}-font`, style.font ?? {}),
    };
}

export function getAllBoardStyleVariables(style: BoardComponentStyleValues) {
    return {
        ...getBoardStyleVariables('board', style.board ?? {}),
        ...getBoardComponentStyleVariables('column', style.column ?? {}),
        ...getBoardComponentStyleVariables('card', style.card ?? {}),
    };
}

export function getDefaultBoardStyle(): BoardComponentStyleValues {
return {
  board: {
    background: {},
    backgroundImage: {},
    foreground: {},
  },
  column: {
    background: {},
    backgroundImage: {},
    foreground: {},
    border: {},
    font: {},
  },
  card: {
    background: {},
    backgroundImage: {},
    foreground: {},
    border: {},
    font: {},
  },
};
}

export function getDefaultColumnStyle() {
return {
  column: {
    background: {},
    backgroundImage: {},
    foreground: {},
    border: {},
    font: {},
  },
  card: {
    background: {},
    backgroundImage: {},
    foreground: {},
    border: {},
    font: {},
  },
};
}

