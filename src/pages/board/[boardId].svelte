<script lang="ts">
  import Board from './_components/Board.svelte';
  import ComponentStyleEditor from './_components/ComponentStyleEditor.svelte';
  import Modal from '../_components/Modal.svelte';
 
  import localforage from 'localforage';

  import { onMount, getContext } from 'svelte';
  import type { Writable } from 'svelte/store';

  import { focus, goto, metatags, params } from '@roxi/routify';

  import { cardIdCounter, columnIdCounter } from '../../stores.js';

  import { apiUrl } from '../../api';
  import { clone, mergeDeep, ChainMap } from '../../util.js';

  import { BackgroundSize } from './constants.js';

  import { getDefaultBoardStyle, getDefaultColumnStyle } from './_components/style.js';

  import { ToastMessageType, addToast } from '../_components/_toast/store';

  export let boardId;

  const mode = $params['mode'];

  let board = null;
  let lastModified = null;

  $: metatags.title = board === null ? 'Board' : board.title;

  /* Board utilities */

  /// Add numerical IDs to columns and cards for internal use
  function addIdsToBoard(board) {
    for (const column of board.columns) {
      column.id = $columnIdCounter;

      for (const card of column.cards) {
        card.id = $cardIdCounter;

        cardIdCounter.increment();
      }

      columnIdCounter.increment();
    }

    return board;
  }

  /// Get the board data minus the IDs used internally
  function getBoardData() {
    if (board === null) return null;

    const boardCopy = clone(board);

    for (const column of boardCopy.columns) {
      delete column.id;

      for (const card of column.cards) {
        delete card.id;
      }
    }

    return boardCopy;
  }

  /* Board loading */
  async function getLocalBoardData() {
      return await localforage.getItem(boardId);
  }

  async function getRemoteBoardData() {
      const response = await fetch(`${apiUrl}/board/${boardId}`, {
          method: 'GET',
          credentials: 'include',
      });

      if (response.ok) {
        addToast({
          type: ToastMessageType.Success,
          message: 'Remote board data retrieved',
          timeout: 2000
        });

        return await response.json();
      } else {
        addToast({
          type: ToastMessageType.Failure,
          message: await response.text(),
          timeout: 5000
        });

        return null;
      }
  }

  let showModifiedModal = false;

  /* Board saving */
  async function saveBoard() {
      const boardData = getBoardData();

      if (mode === 'local') {
          await localforage.setItem(boardId, boardData);
      } else if (mode === 'remote') {
          const response = await fetch(`${apiUrl}/board/${boardId}`, {
              method: 'PATCH',
              credentials: 'include',
              body: JSON.stringify({
                  data: boardData,
                  lastModified: lastModified,
              })
          });

          if (response.ok) {              
              addToast({
                type: ToastMessageType.Success,
                message: 'Remote board saved successfully',
                timeout: 2000
              });
              
              await loadRemoteBoard();
          } else {
              if (response.status === 409) {
                  showModifiedModal = true;

                  addToast({
                    type: ToastMessageType.Failure,
                    message: 'Remote board has been modified by another user',
                    timeout: 5000
                  });
              } else {
                  addToast({
                    type: ToastMessageType.Failure,
                    message: await response.text(),
                    timeout: 5000
                  });
              }
          }
      }
  }

  /* Title editing */
  let editingTitle = false;

  /* Modal logic */
  let showExportModal = false;
  let exportContent = null;
  let exportUrl = null;

  function openModal() {
    const jsonContent = JSON.stringify(getBoardData(), null, 2);

    exportContent = jsonContent;

    const blob = new Blob([jsonContent], {type: 'application/json'});

    exportUrl = URL.createObjectURL(blob);

    showExportModal = true;
  }

  let showCustomizeModal = false;

  let showContributorModal = false;

  let viewOnly = false;

  let contributors: any[] = null;

  async function getContributorsForBoard() {
    const response = await fetch(`${apiUrl}/board/${boardId}/contributors`, {
      method: 'GET',
      credentials: 'include',
    });

    if (response.ok) {
      addToast({
        type: ToastMessageType.Success,
        message: 'Successfully loaded contributors',
        timeout: 3000
      });

      return await response.json();
    } else {
      addToast({
        type: ToastMessageType.Failure,
        message: await response.text(),
        timeout: 5000
      });

      return null;
    }
  }

  let contributorUsername = '';
  let contributorViewOnly = false;

  let contributorError = '';

  async function addContributor() {
    if (contributorUsername.indexOf('#') === -1) {
      return;
    }

    const parts = contributorUsername.split('#');

    if (parts.length !== 2) {
      return;
    }

    const [username, number] = parts;

    const parsedNumber = Number.parseInt(number)

    if (Number.isNaN(parsedNumber)) {
      return;
    }

    const addResponse = await fetch(`${apiUrl}/board/${boardId}/contributors`, {
      method: 'POST',
      credentials: 'include',
      body: JSON.stringify({
        user_name: username,
        user_number: number,
        view_only: contributorViewOnly,
      }),
    });

    if (addResponse.ok) {
      contributors = await getContributorsForBoard();

      addToast({type: ToastMessageType.Success, message: await addResponse.text(), timeout: 3000});
    } else {
      addToast({type: ToastMessageType.Failure, message: await addResponse.text(), timeout: 5000});
    }
  }

  async function removeContributor(userId) {
    const removeResponse = await fetch(`${apiUrl}/board/${boardId}/contributors`, {
      method: 'DELETE',
      credentials: 'include',
      body: JSON.stringify({
        user_id: userId,
      }),
    });

    if (removeResponse.ok) {
      addToast({type: ToastMessageType.Success, message: await removeResponse.text(), timeout: 3000});
      
      contributors = await getContributorsForBoard();
    } else {
      addToast({type: ToastMessageType.Failure, message: await removeResponse.text(), timeout: 5000});
    }
  }

   let boardNotFound = false;
   let public_ = null; 

  async function loadRemoteBoard() {
    const remoteBoard = await getRemoteBoardData();

    if (remoteBoard === null) {
        boardNotFound = true;
        viewOnly = true;
    } else {
        board = addIdsToBoard(JSON.parse(remoteBoard.data));

        board.style ??= getDefaultBoardStyle();

        for (const column of board.columns) {
          column.style ??= getDefaultColumnStyle();
        }

        public_ = remoteBoard.public;
        viewOnly = !remoteBoard.editable;
        lastModified = remoteBoard.modified_datetime;
    }
  }
  
  let remoteUser: Writable<any> = getContext('remoteUser');

   onMount(async () => {
     if (mode === 'local') {
        const localBoard = await getLocalBoardData();

        board = addIdsToBoard(localBoard);

        board.style ??= getDefaultBoardStyle();

        for (const column of board.columns) {
          column.style ??= getDefaultColumnStyle();
        }
     } else if (mode === 'remote') {
        remoteUser.subscribe(async () => {
          await loadRemoteBoard();
  
          if (!viewOnly) {
            contributors = await getContributorsForBoard();
          }
        });
     }
  });


</script>

<style>
  #board-header {
    display: flex;

    align-content: space-between;
  }

  #board-header > h1 {
    margin: 0 0 0.5em 0;
  }

  #title-entry {
    font-size: 2em;
  }

  #board-export-content {
    height: 20rem;

    overflow-y: auto;
  }

  h1:hover {
    background-color: darkgray;

    cursor: pointer;
  }

  table {
    display: grid;

    width: 100%;

    grid-template-columns: 2fr 1fr 2em;
  }

  thead, tbody, tr {
    display: contents;
  }

  td, th {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }

  th {
    text-align: left;
  }
</style>

{#if boardNotFound}
  <p>You do not have access to this board... :(</p>
{:else if board === null}
  <p>Loading...</p>
{:else}
  <header id="board-header">
    {#if editingTitle}
      <input id="title-entry" type="text" bind:value={board.title} on:focusout="{() => editingTitle = false}" use:focus>
    {:else}
      <h1 on:click="{() => editingTitle = true}">{board.title}</h1>
    {/if}
  </header>

  <div>
    {#if mode === 'local' || (mode === 'remote' && !viewOnly)}
      <button on:click={saveBoard}>Save</button>
    {/if}
    <button on:click="{openModal}">Export</button>
    {#if mode === 'local' || (mode === 'remote' && !viewOnly)}
      <button on:click="{() => showCustomizeModal = true}">Customize</button>
    {/if}
    {#if mode === 'remote' && !viewOnly}
      <button on:click="{() => showContributorModal = true}">Contributors</button>
    {/if}
  </div>

  <Board columns={board.columns} style={board.style} />
{/if}

<Modal bind:show="{showExportModal}" width={32}>
  <h2>Export Board</h2>
  {#if exportUrl !== null}
    <a href="{exportUrl}" download="board.json">Download Board</a>
  {/if}
  {#if exportContent !== null}
    <pre id="board-export-content">
      {exportContent}
    </pre>
  {/if}
</Modal>

<Modal bind:show="{showCustomizeModal}" width={24}>
  <h2>Customize</h2>
  <form>
    <ComponentStyleEditor title="Board" bind:style={board.style.board} />
    <ComponentStyleEditor title="Column" bind:style={board.style.column} />
    <ComponentStyleEditor title="Card" bind:style={board.style.card} />
  </form>
</Modal>

<Modal bind:show="{showContributorModal}" width={24}>
  <h2>Contributors</h2>
  <input type="text" bind:value="{contributorUsername}" />
  <button type="button" on:click="{addContributor}">Add</button>
  <label for="contributor-view-only">
    View Only
    <input type="checkbox" name="contributor-view-only" bind:checked={contributorViewOnly} />
  </label>
  <hr />
  {#if contributors === null}
    <p>Loading</p>
  {:else}
    <table>
      <thead>
        <tr>
          <th>Username</th>
          <th>View Only</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {#each contributors as contributor}
          <tr>
            <td>{contributor.name}#{contributor.number}</td>
            <td>{contributor.view_only}</td>
            <td>
              <button type="button" on:click={() => removeContributor(contributor.id)}>X</button>
            </td>
          </tr>
        {/each}
      </tbody>
    </table>
  {/if}
</Modal> 

<Modal show={showModifiedModal} width={16}>
  <h2>Modified on Server</h2>
  <p>
    This board already has a newer version on the server.
    Would you like to reload with the latest data?
  </p>
  <button type="button" on:click={async () => {loadRemoteBoard(); showModifiedModal = false;}}>Yes</button>
  <button type="button" on:click={() => showModifiedModal = false}>No</button>
</Modal>